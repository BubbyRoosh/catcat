# catcat

ctct

## Cat cat :D

Simple cat implementation with -n and -s options.. as an ascii cat :D

## Usage

ctct [-ns] [file ...]

## Options

* -n  Number each line, starting at 1.
* -s  Squeeze empty lines in a row.
```
this





input
```
becomes
```
this

input
```

## Example

```
ctct /etc/passwd - ~/.profile
```
prints the contents of /etc/passwd, then prints stdin until an EOF is sent
(on a tty this is typically ctrl+d), then prints ~/.profile.
