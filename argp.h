#ifndef ARGP_H__
#define ARGP_H__

/* The contents of argv[0] when ARGBEGIN is called */
char *progname;

/* Starts iterating over each argument passed to the program, running each
 * character through a user-defined switch statement if the argument starts
 * with '-'
 */
#define ARGBEGIN for(progname=*argv++,argc--;*argv&&**argv=='-';argv++,argc--){\
                    int used_next = 0;\
                    while (*++*argv)\
                        switch(**argv)

/* Concludes the ARGBEGIN macro, skipping an argument if ARGNEXT is used. */
#define ARGEND     if (used_next && argc > 1) {\
                       argv++;\
                       argc--;\
                   }\
               } /* closes for loop from ARGBEGIN */

/* Gets the next argument and sets it to <str>. If there are no more arguments,
 * the <h> expression is ran and <str> is set to NULL
 */
#define ARGNEXT(str, h) (used_next++,(str)=argc>1?*(argv+1):((h),NULL))

#endif /* ARGP_H__ */
