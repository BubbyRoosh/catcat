DESTDIR=/usr
PREFIX=/local

cat: cat.c
	$(CC) cat.c -o ctct -pedantic

clean:
	rm ./ctct

install: cat
	mkdir -p ${DESTDIR}${PREFIX}/bin
	install -o root -m 4555 ctct ${DESTDIR}${PREFIX}/bin/ctct

uninstall:
	rm ${DESTDIR}${PREFIX}/bin/ctct
